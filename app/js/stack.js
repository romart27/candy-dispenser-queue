var Stack = function() {
	var functionSet=(function() {
		var _elements=[]; // creating a private array
		return [
		// push function
		function()
		{ return _elements.push .apply(_elements,arguments); },
		 // pop function
		function()
		{ return _elements.pop .apply(_elements,arguments); },
		function() { return _elements.length; },
		function(n) { return _elements.length=n; }];
	})();
	this.push=functionSet[0];
	this.pop=functionSet[1];
	this.getLength=functionSet[2];
	this.setLength=functionSet[3];
	// initializing the stack with given arguments
	this.push.apply(this,arguments);
};

var s=new Stack(0,1), e;
s.push(2);
console.log(s.getLength()); // 3
while(undefined!==(e=s.pop()))
	console.log(e); // 2, 1, 0
