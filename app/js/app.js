//Created by : Kid Romart Medinate
//Started Date : Sunday 30, 2017; 5:12 PM
//Deadline Date : Tuestday 1, 2017
//Data Struture on WebApp

$(document).ready(function() {
  //Navigation Menu Actions
  $("li.transaction").click(function() {
    $("section").hide("slow");
    $("#transaction").show("slow");
    $("div.welcome").hide("slow");
  });
  $("li.reports").click(function() {
    $("section").hide("slow");
    $("#reports").show("slow");
    $("div.welcome").hide("slow");
  });
  $("li.history").click(function() {
    $("section").hide("slow");
    $("#history").show("slow");
    $("div.welcome").hide("slow");
  });
  $("li.manage").click(function() {
    $("section").hide("slow");
    $("#manage").show("slow");
    $("div.welcome").hide("slow");
  });
  $("li.about").click(function() {
    $("section").hide("slow");
    $("#about").show("slow");
    $("div.welcome").hide("slow");
  });
  $(".smt-clear").click(function() {
    $("div.middle").css("display","table-cell");
    $("div.alert-data").show("fast");
  });
  $(".smt-yes").click(function() {
    location.reload();
  });
  $(".smt-no").click(function() {
    $("div.middle").css("display","none");
    $("div.alert-data").hide("fast");
  });
  //Transaction Option Actions
  $("#purchase").click(function() {
    $("div.middle").css("display","table-cell");
    $("div.purchase-share").show("fast");
  });
  $("div.pps.btn-next").click(function() {
    var price = $("input.pps").val();
    var units = $("input.pps-units").val();
    try {
      price = Number(price);
      units = Number(units)
    } catch (e) {
      breadcrumbs("Character values are not Applicable")
    }
    if(units<=0||price<=0){
      breadcrumbs("Values less than 1 is not applicable")
    }else {
      var r = transaction(price, units, "p")
      if (r==1) {
        breadcrumbs("Empty Values is not Applicable ")
      }else if (r==2) {
        breadcrumbs("Transaction Unknown : Internal Error")
      }else if (r==4) {
        breadcrumbs("Transaction Success")
      }
      $("div.purchase-share").hide("fast");
      $("div.middle").delay(2000).css("display","none");
      $("input").delay(2000).val("")
    }
  });
  $("#sell").click(function() {
      if (isEmpty(stkUnits)||isEmpty(stkUnits)) {
        breadcrumbs("Purchase First!")
        console.log(stkUnits.length+":"+stkPrice.length);
      }else{
        console.log(stkUnits.length+":"+stkPrice.length);
        $("div.middle").css("display","table-cell");
        $("div.sell-ask").show("fast");
      }
  });

  $(".btn-fifo").click(function() {
    $("div.middle").css("display","table-cell");
    $("div.sell-ask").hide("fast");
    $(".sell-share-fifo").show("fast")
  });
  $("div.smt-fifo").click(function() {
    var price = $("input.fifo").val();
    var units = $("input.fifo-units").val();
    try {
      price = Number(price);
      units = Number(units)
    } catch (e) {
      breadcrumbs("Character values are not Applicable")
    }
    if (isEmpty(stkUnits)||isEmpty(stkUnits)) {
      breadcrumbs("Purchase First!")
    }else{
      if(units<=sharesTotal){
        if(units<=0||price<=0){
          breadcrumbs("Values less than 1 is not applicable")
        }else {
          var r = transaction(price, units, "s-fifo")
          if (r==1) {
            breadcrumbs("Empty Values is not Applicable ")
          }else if (r==2) {
            breadcrumbs("Transaction Unknown : Internal Error")
          }else if (r==5) {
            breadcrumbs("Transaction Success")
          }
          $("div.purchase-share").hide("fast");
          $(".sell-share-fifo").hide("fast")
          $("div.middle").delay(2000).css("display","none");
          $("input").delay(2000).val("")
        }
      }else{
        breadcrumbs("Values higher than max is not applicable")
      }
    }
  })
  $(".btn-lifo").click(function() {
    $("div.middle").css("display","table-cell");
    $("div.sell-ask").hide("fast");
    $(".sell-share-lifo").show("fast")
  })
  $("div.smt-lifo").click(function() {
    var price = $("input.lifo").val();
    var units = $("input.lifo-units").val();
    try {
      price = Number(price);
      units = Number(units)
    } catch (e) {
      breadcrumbs("Character values are not Applicable")
    }
    if (isEmpty(stkUnits)||isEmpty(stkUnits)) {
      breadcrumbs("Purchase First!")
      console.log(stkUnits.length+":"+stkPrice.length);
    }else{
      if(units<=sharesTotal){
        if(units<=0||price<=0){
          breadcrumbs("Values less than 1 is not applicable")
        }else{
          var r = transaction(price, units, "s-lifo")
          if (r==1) {
            breadcrumbs("Invalid Value")
          }else if (r==2) {
            breadcrumbs("Transaction Unknown : Internal Error")
          }else if (r==5) {
            breadcrumbs("Transaction Success")
          }
          $("div.purchase-share").hide("fast");
            $(".sell-share-lifo").hide("fast")
          $("div.middle").delay(2000).css("display","none");
          $("input").delay(2000).val("")
        }
      }else{
        breadcrumbs("Values higher than max is not applicable")
      }
    }
  })

  //Dialog
  $("div.cls-d").click(function() {
    $("div.d-m-center").hide("slow");
    $("div.middle").css("display","none");
    $("input").val("")
  })
});

ripple("li, div.container-box, btn-blue","rgba(3, 169, 244, 0.54)");


//function start

var stkPrice = [];
var stkUnits = [];
var unitsSum = 0;
var day = 0;

var sharesTotal = 0;
var assetsTotal = 0;
var assetSold = 0;
var gainsTotal = 0;
var gainRes = 0;

var gainTotal = 0
var gainTemp = 0
var lossTotal = 0

var purchaseHistoryStr = "";


function isEmpty(array) {
  if(array.length > 0){
    return false;
  }else{
    return true;
  }
}

function breadcrumbs(text) {
  $("div.b-msg").text(text)
  $("div.b-top.success").fadeIn("slow")
  $("div.b-top.success").delay(3000).fadeOut()
}


function transaction(price, units, type) {
  if(price==0||units==0){
    console.log("Invalid input");
    return 1;
  }else{
    if(type=="p"){
      purchase(price, units)
      return 3;
    }else if (type=="s-lifo") {
      gainsTotal = 0;
      lifo_sell(price, units)
      gainTemp = 0;
      assetsTotal = assetsTotal -((price*units)-gainsTotal)
      saleHistory(units, price)
      reportChart("LIFO", price, units, gainsTotal, assetsTotal)
      return 4;
    }else if (type=="s-fifo") {
      gainsTotal = 0;
      fifo_sell(price, units)
      gainTemp = 0
      assetsTotal = assetsTotal -((price*units)-gainsTotal)
      saleHistory(units, price)
      reportChart("FIFO", price, units, gainsTotal, assetsTotal)
      return 5;
    }else{
      console.log("Transaction Unknown");
      return 2;
    }
  }
}


function purchase(price, units) {
  try {
    stkPrice.push(price);
    console.log(price + " has been pushed to Stack Price");
    stkUnits.push(units);
    console.log(units + " has been pushed to Stack Units");
    unitsSum = unitsSum + Number(units);
    console.log(unitsSum + " is the new total of units");
    sharesTotal = sharesTotal + units * 1;
    assetsTotal = assetsTotal + (price * units);
    transactionHistory(units, price)
    transactionReport(units, price)
  } catch (e) {
    console.log("Error: Something went wrong on ["+e+"]");
  }
  Updater()
}

function transactionHistory(units, price) {
  day = day + 1
  $("div.day").css("background-color", "#F44336")
  $("div.day").text(day)
  $("div.h-main-content").append("<p><span>Day "+day+"</span> : Purchased " + units + " share/s worth " + Number(price).toFixed(2) + "/unit. Total Capital : " + Number(price*units).toFixed(2)+"</p></br>")
  purchaseHistoryStr = purchaseHistoryStr + day+";"+price+":"
  console.log(purchaseHistoryStr)
  Updater()
}

function saleHistory(units, price) {
  day = day + 1
  $("div.day").css("background-color", "#F44336")
  $("div.day").text(day)
  var data = ""
  var reason = ""
  if(gainsTotal>=0){
    reason = "Your gained"
    data = "<span class=\"status up\" title=\""+reason+"\"><i class=\"material-icons\">trending_up</i>"+gainsTotal+"</span>";
    gainTotal = gainTotal + gainsTotal;
  }else{
    reason = "Your loss"
    data = "<span class=\"status down\" title=\""+reason+"\"><i class=\"material-icons\">trending_down</i>"+gainsTotal+"</span>"
    lossTotal = lossTotal + (gainsTotal*-1);
  }
  assetSold = ((assetSold*1) + ((Number(price*units).toFixed(2)*1)))
  $("div.h-main-content").append("<p><span>Day "+day+"</span> : Sold " + units + " share/s worth " + Number(price).toFixed(2) + "/unit. Sold Assets : "+assetSold+" "+data+"</p></br>")
  Updater()
}

function transactionBreakDown(new_units,units, new_price, gain, bool) {
  console.log("New Units:"+new_units+"  New Price:"+new_price);
  if (new_units!=0) {
    var splitPurchase = purchaseHistoryStr.split(":");
    var temp = ""+new_price
    var gStr = ""
    var d = 1
    for (var i = 0; i < splitPurchase.length; i++) {
      if(temp==(splitPurchase[i].split(";"))[1]){
        d = (splitPurchase[i].split(";"))[0];
      }
    }
    if ((gain-gainTemp)<0) {
      gStr = "loss"
    }else{
      gStr = "gain"
    }
      if (bool||units==0) {
        $("div.card-data").append("<div>"+
          "Day:"+(day+1)+" "+"From Purchased Day "+d+" deducted "+new_units+" units with  the price of P"+ new_price + " which was totaly sold."+" Having a "+gStr+" of "+ Number((gain-gainTemp)*1) +"."+
          "</div></br>");
      }else {
        $("div.card-data").append("<div>"+
          "Day:"+(day+1)+" "+"From Purchased Day "+d+" deducted "+(new_units-units)+" units with  the price of P"+ new_price + " which partialy sold with the remaing "+ units + "units in Day "+d+"."+" Having a "+gStr+" of "+ Number((gain-gainTemp)*1) +"."+
          "</div></br>");
      }
  }
  gainTemp = gain
}


function transactionReport(units, price) {
  console.log("Purchased Success!");
  console.log("Price per Share Purchased: "+price);
  console.log("Units per Share Purchased: "+units);
  console.log("Shares on Hand: "+ sharesTotal);
  console.log("Assets Purchased Total: "+ (units*price) );
  console.log("Asset Total on Hand: "+ assetsTotal);
  reportChart("Purchase", price, units, "", assetsTotal)
  Updater()
}

function reportChart(method, price, units, gain, assets) {
  $("table.table-chart").append("<tr>"+
    "<td>"+day+"</td>"+
    "<td>"+method+"</td>"+
    "<td>"+price+"</td>"+
    "<td>"+units+"</td>"+
    "<td>"+sharesTotal+"</td>"+
    "<td>"+gain+"</td>"+
    "<td>"+(units*price)+"</td>"+
    "<td>"+assets+"</td></tr>")
}

function Updater() {
  $("i.value").text(sharesTotal)
  $("input.lifo-units").attr("max",sharesTotal);
  chartGainLoss(gainTotal, lossTotal)
  chartAssets(assetsTotal, assetSold)
}


//LIFO

function lifo_sell(price, units) {
  console.log("LIFO Transaction");
 if (sharesTotal>=units) {
   var price_new = stkPrice.pop();
   var units_new = stkUnits.pop();
   var topTotal = (price * units)
   if(Number(units)>Number(units_new)){
     sharesTotal = sharesTotal - units_new;
     console.log("Shares Total: "+sharesTotal);
     gainsTotal = gainsTotal + ((units_new*price) - (units_new*price_new))
     console.log("Gains Total: "+gainsTotal);
     units = units - units_new
     console.log("Units: "+units);
  //   assetsTotal = assetsTotal + (units_new*price_new)
     console.log("Assets Total: "+assetsTotal);
     transactionBreakDown(units_new, units, price_new, gainsTotal, true)
     lifo_sell(price,units)
   }else if (Number(units) <= Number(units_new)) {
     sharesTotal = sharesTotal - units
     console.log("Shares Total: "+sharesTotal);
  //   assetsTotal = assetsTotal + (units*price_new)
     gainsTotal = gainsTotal + ((units*price) - (units*price_new))
     console.log("Gains Total: "+gainsTotal);
     units = units_new - units
     console.log("Units: "+units);
     transactionBreakDown(units_new, units, price_new, gainsTotal, false)
     if(sharesTotal > 0){
       stkUnits.push(units)
       stkPrice.push(price_new)
     }
   }else {
     console.log("You dont have enough to sell")
   }
 }
 Updater()
}


//FIFO



function fifo_sell(price, units) {
  console.log("FIFO Transaction");
  if (sharesTotal>=units) {
    var price_new = stkPrice.shift();
    var units_new = stkUnits.shift();
    var topTotal = (price * units)
    if(Number(units)>Number(units_new)){
      console.log(units+">"+units_new);
      sharesTotal = sharesTotal - units_new;
      console.log("Shares Total "+ sharesTotal);
      gainsTotal = gainsTotal + ((units_new*price) - (units_new*price_new))
      console.log("Gain Total "+ gainsTotal);
      units = units - units_new
      console.log("Units :"+units);
    //  assetsTotal = assetsTotal + (units_new*price_new)
      console.log("Assets Total: "+assetsTotal);
      transactionBreakDown(units_new , units, price_new, gainsTotal, true)
      fifo_sell(price,units)
    }else if (Number(units) <= Number(units_new)) {
      sharesTotal = sharesTotal - units
      console.log("Shares Total "+ sharesTotal);
    //  assetsTotal = assetsTotal + (units*price_new)
      console.log("Assets Total: "+assetsTotal);
      gainsTotal = gainsTotal + ((units*price) - (units*price_new))
      console.log("Gain Total "+ gainsTotal);
      units = units_new - units
      console.log("Units :"+units);
      transactionBreakDown(units_new , units, price_new, gainsTotal, false)
      if(sharesTotal > 0){
        stkUnits.unshift(units)
        stkPrice.unshift(price_new)
      }
    }else {
      console.log("You dont have enough to sell")
    }
  }
  Updater()
}

//function end



//chart Implementation
function chartGainLoss(gain, loss) {
  new Chart(document.getElementById("myChartGain"),{
       "type": "pie",
       "data": {
           "labels": ["Gain : "+gain, "Loss : "+loss],
           "fontColor":'#fff',
           "datasets": [{
               "label": "Gain and Loss Overview",
               "data": [gain,loss],
               "backgroundColor": ["rgb(76, 175, 80)", "rgb(244, 67, 54)"]
           }]
       },
       "options": {
           "responsive": false,
           "legend": {
               "position": "top",
           },
           "title": {
               "display": true,
               "text": "Gain Loss Summary",
               "fontSize":15,
               "fontStyle":"bold",
               "fontColor":'#fff'
           },
           "animation": {
               "animateScale": true,
               "animateRotate": true
           }
       }
   });
}
function chartAssets(assets_total, difference) {
  new Chart(document.getElementById("myChartAssets"),{
       "type": "doughnut",
       "data": {
           "labels": ["Remaining Assets: "+assets_total, "Sold Assets : "+difference],
           "fontColor":'#fff',
           "datasets": [{
               "label": "Assets Overview",
               "data": [assets_total,difference],
               "backgroundColor": ["rgb(76, 175, 80)", "rgb(244, 67, 54)"]
           }]
       },
       "options": {
           "responsive": false,
           "legend": {
               "position": "top",
           },
           "title": {
               "display": true,
               "text": "Assets Summary",
               "fontSize":15,
               "fontStyle":"bold",
               "fontColor":'#fff'
           },
           "animation": {
               "animateScale": true,
               "animateRotate": true
           }
       }
   });
}
