console.log("App Starting...!");
$("button").click(function() {
  if($(".dc-dispense-all").text()!="Cancel"){
    btnEvent($(this).attr("class"))
  }else {
    btnEventAll($(this).attr("class"))
  }
})
$(".d-close").click(function() {
  $(".dialog").fadeOut()
  $(".d-wrapper").removeAttr("style")
})
$(".dc-add-all").click(function() {
  var en = false;
  var count = 0
  count = prompt("How many Candies you wanted to add?", "")
  if(Number(count).toString()!=(NaN).toString()){
    if(count<1||count==0){
      alert("Error: Less than 1 is not Valid!")
      en = true;
    }else {
      if(parseInt(count).toString()==(NaN).toString()){
        en=false
      }else{
        for (var i = 0; i < count; i++) {
          CandyDispenser.push(candyFlavors[Math.floor((Math.random() * 10) + 1)])
        }
        candyAHistory()
        en = true;
      }
    }
  }else {
    alert("Its not a number!")
    en = true;
  }
  if(!en){
    alert("Its not a number!")
  }
})
$(".dc-dispense-all").click(function() {
  if (parseInt($(this).val())==1) {
    $(this).text("Dispense All Candies")
    $(this).removeAttr("style")
    $(".btn-circle").removeAttr("style")
    $("button").removeAttr("disabled")
    $("button").attr("id","btnHover")
    $(this).val(0)
  }else{
    $("button").attr("disabled","disabled")
    $("button").removeAttr("id")
    $(this).removeAttr("disabled")
    $(this).attr("id","btnHover")
    $(this).text("Cancel")
    $(this).attr("style", "background-color: #d50000;color: white;")
    $(".btn-circle").attr("style", "background-color: #d50000;color: white;")
    $(".btn-circle").removeAttr("disabled")
    $(".btn-circle").attr("id","btnHover")
    $(this).val(1)
  }
})
function btnEvent(c) {
  var btnC = " btn-circle"
  var btnR = " btn-regular"
  switch (c) {
    case "dc-add"+btnR:
      addCandy()
      break;
    case "dc-dispense"+btnR:
      dispenseCandy()
      break;
    case "m-0"+btnC:
      mon()
      break;
    case "m-1"+btnC:
      tue()
      break;
    case "m-2"+btnC:
      wed()
      break;
    case "m-3"+btnC:
      tue()
      break;
    case "m-4"+btnC:
      fri()
      break;
    case "m-5"+btnC:
      sat()
      break;
    case "m-6"+btnC:
      sun()
      break;
    default:

  }
}
function btnEventAll(c) {
  var btnC = " btn-circle"
  var btnR = " btn-regular"
  switch (c) {
    case "m-0"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      mon()
    }
      break;
    case "m-1"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      tue()
    }
      break;
    case "m-2"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      wed()
    }
      break;
    case "m-3"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      tue()
    }
      break;
    case "m-4"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      fri()
    }
      break;
    case "m-5"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      sat()
    }
      break;
    case "m-6"+btnC:
    while (!isEmptyArray(CandyDispenser)) {
      sun()
    }
      break;
    default:

  }
  $(".dc-dispense-all").text("Dispense All Candies")
  $(".dc-dispense-all").removeAttr("style")
  $(".btn-circle").removeAttr("style")
  $("button").removeAttr("disabled")
  $("button").attr("id","btnHover")
  daemonCheck()
}
//Queue implementation using Array
var CandyDispenser = []
var CandyDispensed = []
var candyDayHistory = []
//Candy Flavors
var candyFlavors = ["Blue Raspberry", "Chocolate","Cola", "Grape","Green Apple", "Lemon", "Orange", "Peppermint", "Raspberry", "Watermelon"]
function onCreateLoad() {
  daemonCheck()
  ripple("button","rgb(213, 0, 0, 0.54)");
}
//Close Add Candy Button Menu
function addCandy() {
  if($(".dc-add").text()!="Close Menu"){
    $(".form").fadeIn()
    $(".dc-add").text("Close Menu")
    onCreateLoad()
  }else{
    $(".form").fadeOut()
    $(".dc-add").text("Add Candy")
    onCreateLoad()
  }
}
//dispense Candy
function dispenseCandy() {
  CandyDispenser.shift()
  onCreateLoad()
}
//Day Controls
function mon() {
  var temp;
  while(!isEmptyArray(CandyDispenser)) {
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Monday>"+temp);
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Monday")
      candyAHistory()
    }else{

    }
  }
}
function tue() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Tuesday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Tuesday")
      candyAHistory()
    }else{
    }
}
function wed() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Wednesday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Wednesday")
      candyAHistory()
    }else{

    }
}
function thu() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Tuesday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Tuesday")
      candyAHistory()
    }else{

    }
}
function fri() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Friday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Friday")
      candyAHistory()
    }else{

    }
}
function sat() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Saturday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Saturday")
      candyAHistory()
    }else{

    }
}
function sun() {
  var temp;
    if(!isEmptyArray(CandyDispenser)){
      temp = CandyDispenser.shift()
      console.log("Sunday>"+temp);
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispenser.push(CandyDispenser.shift())
      CandyDispensed.unshift(temp)
      candyDayHistory.unshift("Sunday")
      candyAHistory()
    }else{

    }
}
//Check is the Queue is Empty
function isEmptyArray(array){
  if(array==0){
    return true
  }else{
    return false
  }
}
//Returns the length of the Queue
function arrayValue() {
  return CandyDispenser.length
}
//Background Checking if the Queue has been Modified
function daemonCheck() {
  if(isEmptyArray(CandyDispenser)){
    $(".dispense-status").text("Opps, Candy Dispenser is Empty.")
    console.log("Opps, Candy Dispenser is Empty.");
    candyHistory = []
    CandyDispensed = []
    buttonControl(true)
  }else{
    var cs = "Candies"
    var c = "Candy"
    var temp
    if(arrayValue()==1){
      temp = c
    }else{
      temp = cs
    }
    $(".dispense-status").text(arrayValue() + " "+temp+" available!")
    console.log(arrayValue() + " "+temp+" available!");
    buttonControl(false)
  }
}
//Error Prevention Controls
function buttonControl(command) {
  if(command){
    $(".btn-circle, .btn-regular").attr("disabled","true")
    $(".btn-circle, .btn-regular").removeAttr("id")
    $(".btn-circle, .btn-regular").attr("title","Please Add Candy to the Dispenser!")
    $(".dc-add").removeAttr("disabled")
    $(".dc-add-all").removeAttr("disabled")
    $(".dc-add-all").attr("id","btnHover")
    $(".dc-add").attr("id","btnHover")
  }else{
    $(".btn-circle, .btn-regular").removeAttr("disabled")
    $(".btn-circle, .btn-regular").attr("id","btnHover")
    $(".btn-circle, .btn-regular").removeAttr("title")
  }
}
//Candies Creator
function dataList(array) {
    var i = document.createElement("DIV");
    i.setAttribute("class", "chooser")
    i.setAttribute("id", "candies")
    document.getElementsByClassName("form")[0].appendChild(i);

    for (var i = 0; i < array.length; i++) {
      var opt = document.createElement("BUTTON")
      var label = document.createTextNode(array[i])
      opt.setAttribute("value", array[i])
      opt.setAttribute("onclick", "candy_m(this)")
      opt.setAttribute("class", "btn-snipet")
      opt.setAttribute("style", "background-color:"+candyColorChooser(array[i])+"; border-color:"+candyColorChooser(array[i]))
      opt.appendChild(label)
      document.getElementById("candies").appendChild(opt);
    }
    console.log("Candy Buttons Created!");
}
//Enqueue Value
function candy_m(context) {
  CandyDispenser.push(context.value)
  createCandy()
  onCreateLoad()
  console.log("Enqueue value:"+context.value);
}
//Creating a visual type Queue Status
function createCandy() {
  $(".dispenser").empty()
  console.log("Status Queue Cleared!");
  for (var i = 0; i < CandyDispenser.length; i++) {
    if(CandyDispenser[i]!=undefined){
      var candy = document.createElement("DIV")
      var flavor = document.createTextNode(CandyDispenser[i])
      candy.setAttribute("class", "selected-candy")
      candy.setAttribute("style", "background-color:"+candyColorChooser(CandyDispenser[i])+"; border-color:"+candyColorChooser(CandyDispenser[i]))
      candy.appendChild(flavor)
      document.getElementsByClassName("dispenser")[0].appendChild(candy)
    }
  }
  console.log("Status Queue Created!");
}
//Creating Colors
function candyColorChooser(val) {
  switch (val) {
    case "Blue Raspberry":
      return "#1a237e";
      break;
    case "Chocolate":
      return "#3E2723";
      break;
    case "Cola":
      return "#e57373";
      break;
    case "Grape":
      return "#4A148C";
      break;
    case "Green Apple":
      return "#7cb342";
      break;
    case "Lemon":
      return "#fdd835";
      break;
    case "Orange":
      return "#ff5722";
      break;
    case "Peppermint":
      return "#607D8B";
      break;
    case "Raspberry":
      return "#d81b60";
      break;
    case "Watermelon":
      return "#d32f2f";
      break;
    default:
      return 0
  }
}
//Creating Visual type for Dequeue Status
function candyAHistory() {
  $(".dispensed-candy").empty()
  console.log("Candy History Cleared!");
  for (var i = 0; i < CandyDispensed.length; i++) {
    if(CandyDispensed!=undefined&&CandyDispensed[i]!=undefined){
      var candy = document.createElement("DIV")
      var flavor = document.createTextNode(candyDayHistory[i]+" - "+CandyDispensed[i])
      candy.setAttribute("class", "selected-candy")
      candy.setAttribute("style", "background-color:"+candyColorChooser(CandyDispensed[i])+"; border-color:"+candyColorChooser(CandyDispensed[i]))
      candy.appendChild(flavor)
      document.getElementsByClassName("dispensed-candy")[0].appendChild(candy)
    }
  }
  daemonCheck()
  createCandy()
  console.log("Candy History Created!");
}
console.log("App Started!");
console.log("Initializing...");
//Unloading Modules
dataList(candyFlavors)
onCreateLoad()
